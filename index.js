// Bài tập 1: Xuất 3 số theo thứ tự tăng dần
/*
Input: Nhập 3 số:
    num1 = 15
    num2 =3
    num3 = 10
Các bước  thực hiện:
B1: lấy giá trị từ ô input
B2: so sánh giá trị 3 số
B3: in 3 sô ra màn hình theo thứ tự tăng dần
Output: 3<10<15
*/
document.getElementById("txt-sap-xep").addEventListener("click", function(){
    var num1 = document.getElementById("txt-so-1").value*1;
    var num2 = document.getElementById("txt-so-2").value*1;
    var num3 = document.getElementById("txt-so-3").value*1;

    if(num1>=num2){
        if (num2>=num3) {
            document.getElementById("txt-ket-qua").innerHTML=
            `
            <div>
            Thứ tự là: ${num3} < ${num2} < ${num1}
            </div>
            `
        }else{
            if(num1>=num3){
                // console.log("a>c>b");
                document.getElementById("txt-ket-qua").innerHTML=
                `
                <div>
                Thứ tự là: ${num2} < ${num3} < ${num1}
                </div>
                `
            }else{
                // console.log("c>a>b");
                document.getElementById("txt-ket-qua").innerHTML=
                `
                <div>
                Thứ tự là: ${num2} < ${num1} < ${num3}
                </div>
                `
            }
        }
    }else{
        if(num2>=num3){
            if(num1>=num3){
                // console.log("b>a>c");
                document.getElementById("txt-ket-qua").innerHTML=
                `
                <div>
                Thứ tự là: ${num3} < ${num1} < ${num2}
                </div>
                `
            }else{
                // console.log("b>c>a");
                document.getElementById("txt-ket-qua").innerHTML=
                `
                <div>
                Thứ tự là: ${num1} < ${num3} < ${num2}
                </div>
                `
            }
        }else{
            // console.log("c>b>a");
            document.getElementById("txt-ket-qua").innerHTML=
            `
            <div>
            Thứ tự là: ${num1} < ${num2} < ${num3}
            </div>
            `
        }
    }
});



// Bài tập 2: Chương trình chào hỏi
/*
Input: chọn người để chào từ select box: Bố
Các bước thực hiện:
B1: Setvalue cho mỗi lựa chọn
B2: Xuất ra màn hình câu chào theo mỗi value
Output: Xuát ra màn hình: Con chào bố!

*/

document.getElementById("txt-gui-loi-chao").addEventListener("click", function(){
    var getValue = document.getElementById("txt-member").value*1;
    switch(getValue){
        case 0:{
            document.getElementById("txt-loi-chao").innerHTML=
            `
            <div>
            Xin hãy chọn thành viên để gửi lời chào!
            </div>
            `
            break;
        }
        case 1:{
            document.getElementById("txt-loi-chao").innerHTML=
            `
            <div>
            Con Chào Bố!
            </div>
            `
            break;
        }
        case 2:{
            document.getElementById("txt-loi-chao").innerHTML=
            `
            <div>
            Con Chào Mẹ!
            </div>
            `
            break;
        }
        case 3:{
            document.getElementById("txt-loi-chao").innerHTML=
            `
            <div>
            Em Chào Chị!
            </div>
            `
            break;
        }
        case 4:{
            document.getElementById("txt-loi-chao").innerHTML=
            `
            <div>
            Chào Em trai!
            </div>
            `
            break;
        }
    }
});


// Bài tập 3: Đếm số chẵn lẻ

/*
Input: Nhấp vào 3 số:
    so1: 3
    so2: 5
    so3: 6
Các bước thể hiện:
    B1: getElement từ input
        tạo biến count
    B2: xác định số chẵn lẻ: so % 2 
    B3: tăng giá trị count lên 1 với mối số chắn
    B4: in ra màn hình
Output: có 1 số chẵn và 2 số lẻ
*/
document.getElementById("txt-dem").addEventListener("click", function(){
    var so1 = document.getElementById("txt-so-thu-nhat").value*1;
    var so2 = document.getElementById("txt-so-thu-hai").value*1;
    var so3 = document.getElementById("txt-so-thu-ba").value*1;
    var count=null;
    if(so1%2==0){
        count++;
    }
    if(so2%2==0){
        count++;
    }
    if(so3%2==0){
        count++;
    }
    document.getElementById("txt-ket-qua-chan-le").innerHTML=
    `
    <div>
    Có ${count} số chẵn và ${3-count} số lẻ
    </div>
    `
});


// Bài tập 4: Đoán hình tam giác

/*
Input: Nhập 3 cạnh của tam giác
    canh1: 4
    canh2: 5
    canh3: 6
Các bước thực hiện:
B1: lấy giá trị từ input
B2: xác định tam giác theo các cạnh
1. Nếu a == b và b == c thì ba cạnh tam giác bằng nhau, ta có tam giác đều.
2. Nếu chỉ có a == b và b != c hoặc b == c và c != a hoặc c == a và a != b thì ta có tam giác cân.
3. Nếu a * a + b * b == c * c hoặc a * a + c * c == b * b hoặc a * a = b * b + c * c ta có tam giác vuông.
4. Trường hợp còn lại là tam giác thường.
B3: In ra màn hình
Output: tam giác khác 
 */

document.getElementById("txt-du-doan").addEventListener("click", function(){
    var canh1 = document.getElementById("txt-canh-1").value*1;
    var canh2 = document.getElementById("txt-canh-2").value*1;
    var canh3 = document.getElementById("txt-canh-3").value*1;

    if(canh1 == canh2 && canh2 == canh3){
        document.getElementById("txt-ket-qua-tam-giac").innerHTML=
        `
        <div>
        Đây là hình tam giác đều
        </div>
        `
    }else if (canh1==canh2 && canh2!=canh3 || canh2 == canh3 && canh3!=canh1 || canh3==canh1 && canh1!=canh2 ){
        document.getElementById("txt-ket-qua-tam-giac").innerHTML=
        `
        <div>
        Đây là hình tam giác cân
        </div>
        `
    }else if(
        Math.pow(canh1,2)== Math.pow(canh2,2)+Math.pow(canh3,2) ||
        Math.pow(canh2,2)== Math.pow(canh1,2)+Math.pow(canh3,2) ||
        Math.pow(canh3,2)== Math.pow(canh1,2)+Math.pow(canh2,2)
            ){
                document.getElementById("txt-ket-qua-tam-giac").innerHTML=
                `
                <div>
                Đây là hình tam giác vuông
                </div>
                `
    }else{
        document.getElementById("txt-ket-qua-tam-giac").innerHTML=
        `
        <div>
        Tam giác khác
        </div>
        `
    }

});
